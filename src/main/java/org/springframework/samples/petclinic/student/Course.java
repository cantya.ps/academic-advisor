/*
 * Copyright 2012-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.student;

import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PropertyComparator;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.samples.petclinic.model.NamedEntity;
import org.springframework.samples.petclinic.visit.Visit;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.*;

/**
 * Simple business object representing a pet.
 *
 * @author Ken Krebs
 * @author Juergen Hoeller
 * @author Sam Brannen
 */
@Entity
@Table(name = "courses")
public class Course extends NamedEntity {

    @Column(name = "birth_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;

    @ManyToOne
    @JoinColumn(name = "type_id")
    private CourseType type;

    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;

   @ManyToOne
    //@Column(name = "father_id")
    private Course father;

    @ManyToOne
    //@Column(name = "mother_id")
    private Course mother;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "father")
    private List<Course> childFather;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mother")
    private List<Course> childMother;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "courseId", fetch = FetchType.EAGER)
    private Set<Visit> visits = new LinkedHashSet<>();

    public int countChild() { return getChildFather().size() + getChildMother().size(); }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public LocalDate getBirthDate() {
        return this.birthDate;
    }

    public Course getFather() {return this.father; }

    public void setFather(Course father) { this.father= father; }

    public List<Course> getChildFather() { return this.childFather; }

    public void setChildFather(List<Course> childFather) { this.childFather =childFather; }

    public Course getMother() {return this.mother; }

    public void setMother(Course mother) { this.mother= mother; }

    public List<Course> getChildMother() { return this.childMother; }

    public void setChildMother(List<Course> childMother) { this.childMother =childMother; }

    public CourseType getType() {
        return this.type;
    }

    public void setType(CourseType type) {
        this.type = type;
    }

    public Student getStudent() {
        return this.student;
    }

    protected void setStudent(Student student) {
        this.student = student;
    }

 //   protected void setPetParents(PetParents petParents) {this.petParents = petParents;}

    protected Set<Visit> getVisitsInternal() {
        if (this.visits == null) {
            this.visits = new HashSet<>();
        }
        return this.visits;
    }

    protected void setVisitsInternal(Set<Visit> visits) {
        this.visits = visits;
    }

    public List<Visit> getVisits() {
        List<Visit> sortedVisits = new ArrayList<>(getVisitsInternal());
        PropertyComparator.sort(sortedVisits,
                new MutableSortDefinition("date", false, false));
        return Collections.unmodifiableList(sortedVisits);
    }

    public void addVisit(Visit visit) {
        getVisitsInternal().add(visit);
        visit.setCourseId(this.getId());
    }



}
