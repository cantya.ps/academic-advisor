/*
 * Copyright 2012-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.student;

import org.springframework.samples.petclinic.student.PetValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

/**
 * @author Juergen Hoeller
 * @author Ken Krebs
 * @author Arjen Poutsma
 */
@Controller
@RequestMapping("/owners/{ownerId}")
class CourseController {

    private static final String VIEWS_PETS_CREATE_OR_UPDATE_FORM = "pets/createOrUpdatePetForm";
    private final CourseRepository courses;
    private final StudentRepository students;

    public CourseController(CourseRepository courses, StudentRepository students) {
        this.courses = courses;
        this.students = students;
    }

    @ModelAttribute("petz")
    public Collection<Course> populatePet() {
        return this.courses.findAll();
    }

    @ModelAttribute("types")
    public Collection<CourseType> populatePetTypes() {
        return this.courses.findCourseTypes();
    }

    @ModelAttribute("student")
    public Student findOwner(@PathVariable("studentId") int studentId) {
        return this.students.findById(studentId);
    }

    @InitBinder("student")
    public void initOwnerBinder(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }

    @InitBinder("course")
    public void initPetBinder(WebDataBinder dataBinder) {
        dataBinder.setValidator(new PetValidator());
    }

    @GetMapping("/pets/new")
    public String initCreationForm(Student student, ModelMap model) {
        Course course = new Course();
        student.addCourse(course);
        model.put("course", course);
        return VIEWS_PETS_CREATE_OR_UPDATE_FORM;
    }

    @PostMapping("/pets/new")
    public String processCreationForm(Student student, @Valid Course course, BindingResult result, ModelMap model) {
        if (StringUtils.hasLength(course.getName()) && course.isNew() && student.getCourse(course.getName(), true) != null){
            result.rejectValue("name", "duplicate", "already exists");
        }
        student.addCourse(course);
        if (result.hasErrors()) {
            model.put("course", course);
            return VIEWS_PETS_CREATE_OR_UPDATE_FORM;
        } else {
            this.courses.save(course);
            return "redirect:/owners/{ownerId}";
        }
    }

    @GetMapping("/pets/{petId}/edit")
    public String initUpdateForm(@PathVariable("courseId") int courseId, ModelMap model) {
        Course course = this.courses.findById(courseId);
        model.put("course", course);
        return VIEWS_PETS_CREATE_OR_UPDATE_FORM;
    }

    @PostMapping("/pets/{petId}/edit")
    public String processUpdateForm(@Valid Course course, BindingResult result, Student student, ModelMap model) {
        if (result.hasErrors()) {
            course.setStudent(student);
            model.put("course", course);
            return VIEWS_PETS_CREATE_OR_UPDATE_FORM;
        } else {
            student.addCourse(course);
            this.courses.save(course);
            return "redirect:/owners/{ownerId}";
        }
    }

}
